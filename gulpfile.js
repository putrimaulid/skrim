'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

var paths = {
	srcCSS: './scss/style.scss',
	destCSS: './app/assets/css/',
	srcBS: 'node_modules/bootstrap/dist/js/bootstrap.min.js',
	srcJQ: 'node_modules/jquery/dist/jquery.min.js',
	srcPP: 'node_modules/popper.js/dist/umd/popper.min.js',
	destJS: './app/assets/js'
}

function style() {
	return (
		gulp
		.src(paths.srcCSS)
		.pipe(sass({
			outputStyle: "compressed"
		}))
		.on('error', sass.logError)
		.pipe(gulp.dest(paths.destCSS))
		.pipe(browserSync.stream())
	);
}

function script() {
	return (
		gulp
		.src([paths.srcBS, paths.srcJQ, paths.srcPP])
		.pipe(gulp.dest(paths.destJS))
		.pipe(browserSync.stream())
	);
}

function serve() {
	browserSync.init({
		server: {
			baseDir: "./app"
		}
	});
	gulp.watch("./app/assets/css/style.css", style());
	gulp.watch("./scss/**/**/*.scss", style());
	gulp.watch(['app/*.html', 'app/assets/img/**/*', 'app/assets/js/**/*.js']).on('change', browserSync.reload);
	script()
}

function defaultTask() {
	return (
		serve()
	);
}

exports.serve = serve;
// exports.default = defaultTask;